<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Voedselbank') }}</title>
    {{-- favicon --}}

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div class="container">
        {{-- header and nav --}}
        <div class="header">
            <nav class="navbar justify-content-between">
                <a class="navbar-brand" href="/">
                    <img src="{{ asset('img/logo.svg') }}" alt="ham">
                </a>
            </nav>
            <div class="collapse" id="navbarToggleExternalContent">
                <div class="bg-dark p-4">
                    <h4 class="text-white">Menu content</h4>
                    <span class="text-muted">Menu inhoud</span>
                </div>
            </div>
        </div>
        {{-- content section --}}
        <main class="py-4">
            @yield('content')
        </main>
        {{-- footer --}}
        <footer class="footer text-muted">
            <div class="container footer-wrap">
                <div class="row align-content-start justify-content-center top-footer">
                    <img class="footer-logo" src="{{ asset('img/footer_logo.svg') }}" alt="logo-foot">
                </div>
                <div class="row align-content-end justify-content-center sub-footer">
                    <br>
                    <p>© 2021 Voedselbank Utrecht – KvK 67822037 – ANBI – RSIN 857187016</p>
                </div>
            </div>
        </footer>
    </div>
</body>

</html>