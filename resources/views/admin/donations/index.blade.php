@extends('layouts.admin')

@section('content')
<div class="container-sm">
    <div class="jumbotron">
        <button type="button" onclick="show()" class="btn btn-success gen-sum">Totaal bedrag</button>
        <hr>
        <h2 class="h2">Totaal donaties afgelopen 30 dagen: <span id="sum">€{{ $sum }}</span></h2>
      </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
    <div class="row justify-content-center">
        <div class="table-responsive custom-tbl">
            <table class="table table-dark table-responsive">
                <tr>
                    <th>Nummer</th>
                    <th>@sortablelink('donation', 'Bedrag')</th>
                    <th width="110px">Begin-datum</th>
                    <th width="110px">Eind-datum</th>
                    <th>Email</th>
                    <th>Niewsbrief</th>
                    <th width="110px">@sortablelink('created_at', 'Datum/Tijd')</th>

                    <th width="280px">Email aanpassen</th>
                </tr>
                @foreach ($donations as $donation)
                <tr>
                    <td>{{ $donation->id }}</td>
                    <td>€{{ $donation->donation }}</td>
                    <td>
                        {{ $donation->created_at->format('Y-m-d') }}
                    </td>
                    <td>
                        @if ($donation->donation_type == 'Eenmalige donatie')
                            Eenmalig
                        @else
                            @if($donation->donation_type == 'Maandelijks')
                                {{ $donation->created_at->addDays(30)->format('Y-m-d') }}  
                            @elseif($donation->donation_type == 'Elk kwartaal')
                                {{ $donation->created_at->addDays(91)->format('Y-m-d') }}  
                            @else
                                {{ $donation->created_at->addDays(365)->format('Y-m-d') }}
                            @endif
                            
                        @endif
                    </td>
                    <td>
                        {{-- get email from email table based on id --}}
                        @if ($donation->newsletter_subscription == 1 )
                            @foreach($emails->where('id', $donation->email_id) as $email)
                                {{ $email->email }}
                            @endforeach
                        @else
                            geen
                        @endif
                    </td>
                    <td>
                        @if ($donation->newsletter_subscription == 1 )
                            Ja
                        @else
                            Nee
                        @endif
                    </td>
                    <td>{{ $donation->created_at->format('Y-m-d H:i') }}</td>
                    <td>
                        <form action="{{ route('donation.destroy',$donation->id) }}" method="POST">
                            <a class="btn btn-primary" href="{{ route('donation.edit',$donation->id) }}">Bewerk</a>
                        </form>
                    </td>
                </tr>
                @endforeach
            </table>
            {!! $donations->appends(\Request::except('page'))->render() !!}
        </div>
    </div>
</div>
<script>
    //show on click
    function show() {
        var x = document.getElementById("sum");
        if (x.style.display === "none") {
        x.style.display = "inline";
        } else {
        x.style.display = "none";
        }
    }
</script>
@endsection
