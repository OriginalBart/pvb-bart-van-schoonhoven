@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Bewerk donation</h2>
            </div>
            <div class="pull-right back-btn">
                <a class="btn btn-outline-primary" href="{{ route('donation.index') }}">Terug</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('donation.update',$donation->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="row">

            <div class="col-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Email:</strong>
                    <input type="email" name="email" value="{{ $donation->email['email'] }}" class="form-control" placeholder="Email">
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Bewerk</button>
            </div>
        </div>

    </form>
</div>
    
@endsection

