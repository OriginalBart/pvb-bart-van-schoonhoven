@extends('layouts.admin')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <ul class="col-12 list-group list-group-flush d-flex justify-content-center">
            <li class="list-group-item">
                <h2 class="h3">Bekijk alle donaties</h2>
                <a href="/donation">
                    <button type="button" class="btn btn-primary">Donations</button>
                </a>
            </li>
            <li class="list-group-item">
                <h2 class="h3">Wijzig de content</h2>
                <a href="/content">
                    <button type="button" class="btn btn-primary">Content</button>
                </a>
            </li>
            <li class="list-group-item">
                <h2 class="h3">Bekijk alle producten</h2>
                <a href="/product">
                    <button type="button" class="btn btn-primary">Products</button>
                </a>
            </li>
        </ul>


    </div>
</div>
@endsection