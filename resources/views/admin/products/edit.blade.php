@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                Bewerk product
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                @endif
                <form method="post" action="{{ route('product.update', $product->product_id) }}">
                    <div class="form-group">
                        @csrf
                        @method('PATCH')
                        <label for="product_name">Product naam:</label>
                        <input type="text" class="form-control" name="product_name" value="{{ $product->product_name }}"/>
                    </div>
                    <div class="form-group">
                        <label for="product_price">Product prijs :</label>
                        <input type="number" step='0.01' value='0.00' class="form-control" name="product_price"  value="{{ $product->product_price }}" />
                    </div>
                    <div class="form-group">
                        <label for="merk">Merk:</label>
                        <input type="text" class="form-control" name="merk" value="{{ $product->merk }}"/>
                    </div>
                    <div class="form-group">
                        <label for="product_price_excl_tax">Product prijs excl btw :</label>
                        <input type="number" step='0.01' value='0.00' class="form-control" name="product_price_excl_tax" value="{{ $product->product_price_excl_tax }}"/>
                    </div>

                    <div class="form-group" {{ $errors->has('filename') ? 'has-error' : '' }}>
                        <label for="filename"></label>
                        <input type="file" name="filename" id="filename" class="form-control">
                        <span class="text-danger"> {{ $errors->first('filename') }}</span>
                    </div>
                    <button type="submit" class="btn btn-primary">bewerk product</button>
                </form>
            </div>
        </div>
    </div>
@endsection
