@extends('layouts.admin')
@section('content')
    <div class="container mt-5">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br/>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
                @php
                    Session::forget('success');
                @endphp
            </div>
        @endif
        <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="product_name">Product name</label>
                <input type="text" class="form-control" name="product_name" placeholder="productnaam"/>
            </div>
            <div class="form-group">
                <label for="product_price">product price :</label>
                <input type="number" step='0.01' value='0.00' placeholder='0.00' class="form-control" name="product_price"/>
            </div>
            <div class="form-group">
                <label for="merk"> merk :</label>
                <input  type="text" class="form-control" name="merk" placeholder="merk">
            </div>
         
            <div class="form-group">
                <label for="product_price_excl_tax">product price excl tax :</label>
                <input type="number" step='0.01' value='0.00' placeholder='0.00' class="form-control" name="product_price_excl_tax"/>
            </div>

            <div class="form-group">
                <div class="card ">


                    <div class="card-body">



                        <div class="form-group" {{ $errors->has('filename') ? 'has-error' : '' }}>
                            <label for="filename"></label>
                            <input type="file" name="filename" id="filename" class="form-control">
                            <span class="text-danger"> {{ $errors->first('filename') }}</span>
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-success btn-md"> Upload</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
