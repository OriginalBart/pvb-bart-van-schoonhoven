@extends('layouts.admin')
@section('content')

<div class="container">
    <div class="row create-wrap justify-content-center">
        <a href=" {{ route('product.create') }} " class="col btn btn-primary btn-sm create-button"> Add More </a>
    </div>
    <div class="row justify-content-center">
        @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        @endif
        <div class="table-responsive custom-tbl">
            <table class="table  table-responsive">
                <thead>
                    <tr>
                        <td scope="col">ID</td>
                        <td scope="col">Productnaam</td>
                        <td scope="col">Productprijs</td>
                        <td scope="col">merk</td>
                        <td scope="col">product_price_excl_tax</td>
                        <td scope="col">Image</td>
                        <td scope="col">Actions</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($product as $product)
                    <tr>
                        <td scope="row">{{$product->id}}</td>
                        <td>{{$product->product_name}}</td>
                        <td>€{{$product->product_price}}</td>
                        <td>{{$product->merk}}</td>
                        <td>€{{$product->product_price_excl_tax}}</td>
                        <td>
                            <img src="/productimages/{{ $product->filename }} " class="img-table">
                        </td>
                        <td><a href="{{ route('product.edit', $product->product_id)}}" class="btn btn-primary">Edit</a>
                        </td>
                        <td>
                            <form action="{{ route('product.destroy', $product->product_id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endsection