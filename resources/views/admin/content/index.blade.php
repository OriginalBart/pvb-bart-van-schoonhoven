@extends('layouts.admin')

@section('content')

<div class="container">
    @foreach ($content as $content)
        <form action="{{ route('content.destroy',$content->id) }}" method="POST">
            <a class="btn btn-primary" href="{{ route('content.edit',$content->id) }}">Bewerk</a>
        </form>
    @endforeach
    <hr>
    <div class="row head-text">
        <h1 class="col-12">{{ $content->title }}</h1>
        <p class="col-12">{{ $content->subtitle }}</p>
    </div>
    <div>
        {!! $content->content !!}
    </div>
</div>

@endsection