@extends('layouts.admin')

@section('content')
<div class="container">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div><br />
    @endif
    <div class="pull-right back-btn">
        <a class="btn btn-outline-primary" href="{{ route('content.index') }}">Terug</a>
    </div>
    <form method="post" action="{{ route('content.update', $content->id) }}">
        <div class="form-group">
            @csrf
            @method('PUT')
            <label for="product_name">Kop tekst</label>
            <input type="text" class="form-control" name="title" value="{{ $content->title }}" />
        </div>
        <div class="form-group">
            <label for="product_name">Sub tekst</label>
            <input type="text" class="form-control" name="subtitle" value="{{ $content->subtitle }}" />
        </div>
        <div class="form-group">
            <label for="merk">Content</label>
            {{-- <input type="text" class="form-control" name="content" value="{{ $content->content }}"/> --}}
            <textarea style="height:50vh;" class=" form-control" name="content">
                    {{ $content->content }}
                </textarea>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-primary">Bewerk content</button>
    </form>
</div>
@endsection