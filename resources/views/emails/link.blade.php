<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>mail</title>
</head>

<body>
    We hebben uw donatie ontvangen {{ $name }}
    Hier is uw link <a href="http://localhost:8000/inzage-donatie/{{ $donatie_id }}">Bekijk donatie</a>
</body>

</html>