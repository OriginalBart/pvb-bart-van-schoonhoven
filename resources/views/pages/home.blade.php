@extends('layouts.default')

@section('content')
<section class="container-fluid banner">
    <img class="img-fluid" src="{{ asset('img/banner.jpg') }}" alt="banner">
</section>
<section class="container-fluid content">
    <div class="row head-text">
        <h1 class="col-12">{{ $content->title }}</h1>
        <p class="col-12">{{ $content->subtitle }}</p>
    </div>
    <div>
        {!! $content->content !!}
    </div>
    <a href="/geld-doneren">Klik hier om naar de donatie pagina te gaan</a>
</section>
@endsection