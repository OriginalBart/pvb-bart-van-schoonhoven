@extends('layouts.default')

@section('content')
<section class="container-fluid banner">
    <img class="img-fluid" src="{{ asset('img/banner.jpg') }}" alt="banner">
</section>
<section class="container-fluid content">
    <div class="row head-text">
        <h1 class="col-12">Geld Doneren</h1>
        <p class="col-12">Behalve voedsel kunnen we ook financiële donaties heel goed gebruiken. Doneren kan op
            rekeningnummer: NL34TRIO0338521569 t.n.v. Stichting Voedselbank Utrecht</p>
    </div>
    <h2>Doneer direct</h2>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe, a. Unde ipsum, adipisci, laudantium molestiae
        mollitia voluptatibus amet alias aperiam dolores possimus cumque odit dolor rem sequi quaerat eos fugit.</p>
</section>
<section class="container-fluid form">
    <form action="/link" method="POST">
        @csrf
        @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        @error('bedrag')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        @error('Radio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        @error('note')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="ormControlSelect">Kies uw donatie type: *</label>
            <select name="type" class="form-control" id="FormControlSelect" required>
                <option>Eenmalige donatie</option>
                <option>Maandelijks</option>
                <option>Elk kwartaal</option>
                <option>Jaarlijks</option>
            </select>
        </div>
        <div class="form-group">
            <label for="InputName">Uw Naam: *</label>
            <input type="name" name="name" class="form-control" id="InputName" aria-describedby="Naam"
                placeholder="Naam" required>
        </div>
        <div class="form-group">
            <label for="email">Uw E-mailadres: (om uw donatie later in te kunnen zien)</label>
            <input type="email" name="email" class="form-control" id="email" aria-describedby="email"
                placeholder="Email">
        </div>
        <div class="form-check">
            <input class="form-check-input" name="news" type="checkbox" value="1" id="defaultCheck">
            <label class="form-check-label" for="defaultCheck">
                Ja, schrijf mij in op de nieuwsbrief
            </label>
        </div>
        <label for="bedragInput">Bedrag in euro's: *</label>
        <div class="form-group">
            <input class="form-control" type="number" step='0.01' value='0.00' name="bedrag" id="bedragInput"
                placeholder="1,00" required>
        </div>
        <div class="form-group">
            <label for="">Betalingswijze: *</label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="payment" id="Radios1" value="ideal" checked required>
            <label class="form-check-label" for="Radios1">
                IDEAL
            </label>
        </div>
        <div class="form-group">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Opmerking:</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="note"></textarea>
        </div>
        <button type="submit" class="btn btn-primary"><u>Doneer</u></button>
    </form>
</section>
@endsection