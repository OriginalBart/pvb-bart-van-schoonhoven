@extends('layouts.default')

@section('content')
<section class="container-fluid content">
    <div class="row head-text">
        <h1 class="col-12">We hebben uw donatie ontvangen!</h1>
    </div>
    <hr>
    <section class="container-fluid viewdonation">
        <h2>Uw donatie: {{ $donation->donation }}</h2>
        <h2>Uw donatie kan uitgegeven worden aan:</h2>
        {{-- loop random products --}}
        @foreach ($filtered as $product)
        <div class="product justify-content-center row align-items-center">
            <div class="col-12">
                <img src="/productimages/{{ $product['filename'] }}" alt="">
            </div>
            <div class="col-12">
                <h2> - {{ $product['product_name'] }} - {{ $product['product_price'] }}</h2>
            </div>
        </div>
        @endforeach
    </section>
</section>
@endsection