@extends('layouts.default')

@section('content')
<section class="container-fluid content">
    <div class="row head-text">
        <h1 class="col-12">Bedankt voor uw donatie!</h1>
    </div>
    <hr>
    <h2>Uw link:</h2>
    <a href="http://localhost:8000/inzage-donatie/{{ $donation_id }}">Donatie link</a>
</section>
@endsection