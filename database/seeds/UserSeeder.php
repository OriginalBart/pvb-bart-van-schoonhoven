<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' => '1','name' => 'admin',
                'email' => 'admin@admin.com',
                'username' => 'admin',
                'email_verified_at' => NULL,
                'is_admin' => NULL,
                'password' => bcrypt('123456'),'remember_token' => NULL,'created_at' => '2021-06-13 13:57:03','updated_at' => '2021-06-13 13:57:03'
            ],
        ]);
    }
}