<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'product_id' => '1','product_name' => 'Zak winter wortelen','product_price' => '0.95','product_price_excl_tax' => '0.77','merk' => 'Lokale boer','filename' => 'wortels.png'
            ],
            [
                'product_id' => '2','product_name' => 'Kipfilet','product_price' => '3.14','product_price_excl_tax' => '2.60','merk' => 'gwoon','filename' => 'kipfilet.png'
            ],
            [
                'product_id' => '3','product_name' => 'Zak kruimige aardappels','product_price' => '2.98','product_price_excl_tax' => '2.47','merk' => 'altena','filename' => 'aardappels.png'
            ],
            [
                'product_id' => '4','product_name' => 'Frans Stokbrood','product_price' => '0.90','product_price_excl_tax' => '0.74','merk' => 'AH','filename' => 'stokbrood.png'
            ],
        ]);
    }
}
