<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // calls seeder
        $this->call(ProductSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ContentSeeder::class);
    }
}
