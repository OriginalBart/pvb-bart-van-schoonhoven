<?php

use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contents')->insert([
            [ 'id' => '1','title' => 'Voedselbank Overvecht','subtitle' => 'Welkom op de pagina van Utrecht Overvecht','content' => '<h2>Over Utrecht Overvecht</h2>

            <p><em>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eleifend elit at purus molestie, id laoreet massa gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Duis interdum pharetra lobortis. Suspendisse hendrerit arcu eget lectus sagittis, sed rutrum metus aliquam. Quisque nisi urna, ultrices et convallis at, aliquet ac mi. Sed vitae rutrum tortor. Maecenas in tellus id justo aliquam pulvinar ac et ligula. Nam vel facilisis nunc.</em></p>

            <h2>Maak een donatie</h2>

            <p><strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eleifend elit at purus molestie, id laoreet massa gravida. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Duis interdum pharetra lobortis. Suspendisse hendrerit arcu eget lectus sagittis, sed rutrum metus aliquam. Quisque nisi urna, ultrices et convallis at, aliquet ac mi. Sed vitae rutrum tortor. Maecenas in tellus id justo aliquam pulvinar ac et ligula. Nam vel facilisis nunc.</strong></p>','created_at' => NULL,'updated_at' => '2021-06-13 14:06:55'
            ],
        ]);
    }
}
