<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $table = 'emails';

    public $primaryKey = 'id';

    public $incrementing = true;

    public $timestamps = true;

    public function donation()
    {
        //relaties
        return $this->hasMany('App\Donation', 'email_id');
    }
}
