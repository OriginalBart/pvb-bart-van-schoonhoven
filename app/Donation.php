<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Donation extends Model
{
    use Sortable;
    protected $table = 'donations';

    public $primaryKey = 'id';

    public $timestamps = true;
    //toevoegen aan sortable
    public $sortable = ['donation', 'created_at'];
    //relaties
    public function email()
    {
        return $this->belongsTo('App\Email', 'id');
    }
}
