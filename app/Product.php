<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['product_name','product_price','product_price_excl_tax', 'merk','filename'];

    protected $primaryKey = 'product_id';
}
