<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Donation;
use App\Email;
use App\Product;
use Mail;
use Illuminate\Support\Facades\DB;

class DonateController extends Controller
{
    // donate 

    function donate(Request $req){
        
        //validate form
        $this->validate($req, [
            'name' => 'required',
            'email' => 'sometimes|nullable|email:rfc,dns',
            'bedrag' => 'required|numeric|min:1|max:1000'
        ]);

        // create donation

        //check email
        $email = Email::where('email', '=', $req->input('email'))->first();
        if ($email === null) {
            // mail doesn't exist

            //add new email
            $newemail = new Email;
            $newemail->email = $req->input('email');
            $newemail->save();

            //get email id
            $email_id = Email::where('email', '=', $req->input('email'))->first();

            //add donation
            $donation = new Donation;
            $donation->donation = $req->input('bedrag');
            $donation->email_id = $email_id->id;
            $donation->donation_type = $req->input('type');
            $donation->newsletter_subscription = $req->input('news');
            $donation->payment_method = $req->input('payment');
            $donation->comment = $req->input('note');
            $donation->save();

        }
        else{
            //get old email
            $oldemail = Email::where('email', '=', $req->input('email'))->first();

            //add donation
            $donation = new Donation;
            $donation->donation = $req->input('bedrag');
            $donation->email_id = $oldemail->id;
            $donation->donation_type = $req->input('type');
            $donation->newsletter_subscription = $req->input('news');
            $donation->payment_method = $req->input('payment');
            $donation->comment = $req->input('note');
            $donation->save();
        }

        //

        //mail
        // check newsletter checkbox
        $newsletter = $req->input('news');
        // get donation id
        $donation_id = $donation->id;
        if ($newsletter == 1 ){
            Mail::send('emails.link',[
                'name' => $req->name,
                'donatie_id' => $donation_id
            ], function($mail) use($req){
                $mail->from('bartvanschoonhoven@gmail.com');
                $mail->to($req->email)->subject('donatie link');
            });
            // return message for mail
            return 'U heeft een mail ontvangen met een link om uw donatie te bekijken';
        }
        else{
            // return link page
            return view('pages.showlink', compact('donation_id'));
        }
    }
    public function show($id)
    {
        //get donation by id
        $donation = Donation::find($id);

        // calc products
        // set minimal amount of products
        $min = 1;
        //set maximum amount of products
        $max = 20;
        //get random number from min to max
        $total = rand($min,$max);
        //set budget as donation amount
        $budget = $donation->donation;
        //set spent to 0 
        $spent = 0;

        //loop calc until query fails
        for($i = 0; $i < $total - 1; $i++) {
            $getRandomProduct = Product::where('product_price', '<=' , $budget-$spent/$total - $i)->inRandomOrder()->limit(1)->get();
            if(count($getRandomProduct)) {
                $randomProduct = $getRandomProduct->first();
                $productList[] = $randomProduct;
                $spent += $randomProduct->product_price;
            } else {
                break;
            }
        }
        $getRandomProduct = Product::where('product_price', '<=' , $budget-$spent)->inRandomOrder()->limit(1)->first();
        //fill array with products
        $productList[] = $getRandomProduct;
        //filter array if NULL
        $filtered = array_filter($productList, function($value) { return !is_null($value) && $value !== 'NULL'; });
        //return view
        return view('pages.viewdonation', compact('donation', 'filtered'));
    }
}