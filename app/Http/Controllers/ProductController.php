<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        // get all products
        $product = Product::all();
        return view('admin.products.index', compact('product'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {  
        // display the create view
        return view('admin.products.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // product validation
        $validator = Validator::make($request->all(),
            ['filename' => 'required | mimes:jpeg,png,jpg,bmp|max:1000048',
                'product_name' => 'required| max:2048',
                'product_price' => 'required',
                'product_price_excl_tax' => 'required',
                'merk' => 'required',

            ]

        );

        // if validation fails
        if ($validator->fails()) {
            return back()->withErrors($validator->errors());
        }

        // if validation product success
        if ($product = $request->file('filename')) {

            $name = time() . time() . '.' . $product->getClientOriginalExtension();

            $target_path = public_path('/productimages/');

            if ($product->move($target_path, $name)) {
                // save file name in the database
                $product = Product::create(['filename' => $name,
                    "product_name" => $request->get('product_name'),
                    "product_price" => $request->get('product_price'),
                     "merk" => $request->get('merk'),
                    "product_price_excl_tax" => $request->get('product_price_excl_tax')]);
                $product->save();
            }

        }

        return redirect('/product')->with("success", "Nieuw product toegevoegd");

    }

    /**
     *
     *
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        return view('admin.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'filename' => 'required | mimes:jpeg,png,jpg,bmp|max:1000048',
            'product_name' => 'required| max:2048',
            'product_price' => 'required| max:2048',
            'product_price_excl_tax' => 'required| max:2048',
            'merk' => 'required',
        ]);
        Product::whereId($id)->update($validatedData);

        return redirect('/product')->with('success', 'Product is geweizigd');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return redirect('/product')->with('success', 'Product is verwijderd');
    }

}
