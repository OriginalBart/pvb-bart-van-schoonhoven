<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;

class ContentController extends Controller
{
    public function index(){
        $content = Content::all();
        return view('admin.content.index', compact('content'));
    }
    public function edit($id){
        $content = Content::findOrFail($id);
        return view('admin.content.edit', compact('content'));
    }
    public function update(Request $req, $id){
        $validatedData = $req->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'content' => 'required',
        ]);

        $content = Content::find($id);
        $content->title= $req->title;
        $content->subtitle= $req->subtitle;
        $content->content= $req->content;
        $content->save();

        return redirect('/content')->with('success', 'Content is geweizigd');
    }
    public function show()
    {
        // fetch all the images
        $content = Content::find(1);
        return view('pages.home', compact('content'));
    }
    public function destroy($id)
    {
        $content = Content::findOrFail($id);
        $content->delete();

        return redirect('/content')->with('success', 'Content is verwijderd');
    }
}