<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Donation;
use App\Email;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class adminController extends Controller
{
    Public function index()
    {
        // fetch all the images
        $donations = Donation::sortable()->paginate(10);
        $emails = Email::all();

        //total past 30 days
        $sum = 0;
        // set date to past 30 days
        $date = \Carbon\Carbon::today()->subDays(30);
        $count = Donation::where('created_at','>=',$date)->get();
        foreach($count as $donation)
        {
        $sum += $donation->donation;
        }

        return view('admin.donations.index', compact('donations', 'emails', 'sum'));
    }

    public function edit($id)
    {
        $donation = Donation::findOrFail($id);
        return view('admin.donations.edit', compact('donation'));
    }

    public function update(Request $req, $id)
    {
        $validatedData = $req->validate([
            'email' => 'sometimes|nullable|email:rfc,dns',
        ]);
        $donation = Donation::findOrFail($id);
        $email_id = $donation->email['id'];
        Email::whereId($email_id)->update($validatedData);

        return redirect('/donation')->with('success', 'Email is geweizigd');
    }
}
