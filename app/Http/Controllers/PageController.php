<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    // set homepage to index
    public function index(){
        return view('pages.home');
    }
    // set donate page
    function donate(){
        return view('pages.donate');
    }
    // view donation for dev
    public function viewDonation(){
        return view('pages.viewdonation');
    }
}
