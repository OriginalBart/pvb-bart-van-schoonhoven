<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// auth routes - login
Auth::routes(
    [
        //disable register
        'register' => false,
        'reset' => false,
        'verify' => false,
    ]
);

// page routes
    //home
    Route::get('/', 'ContentController@show');
    //donate
    Route::get('/geld-doneren', 'PageController@donate');
    //weergeven link
    Route::post('/link', 'DonateController@donate');
    //view danation
    Route::get('/inzage-donatie', 'PageController@viewDonation');
    //route voor unieke link
    Route::get('/inzage-donatie/{id}', 'DonateController@show');


    // admin routes
    Route::group(['middleware' => ['auth']], function () {
        Route::get('/admin', [App\Http\Controllers\HomeController::class, 'index'])->name('admin.index');
        // admin contoller
        Route::resource('product', 'ProductController');
        // admin donaties
        Route::resource('donation', 'adminController');
        // admin content
        Route::resource('content', 'ContentController');
    });
